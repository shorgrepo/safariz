<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shorg\ArticleBundle\Entity\Article;
use Shorg\ArticleBundle\Form\Type\ArticleFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/blog", name="test")
 */
class ArticleController extends Controller
{
    /**
     * @Route("/", name="article_list")
     */
    public function listAction()
    {
        // GET LIST ARTICLE
        $articles = $this->get('shorg.article')->listArticle();

        // RETURN
        return $this->render(
            'AppBundle:blog/article:list.html.twig',
            array(
                'articles' => $articles,
            )
        );

    }

    /**
     * @Route("/show/{id}", name="article_show")
     *
     * @param $id
     *
     */
    public function showAction($id)
    {
        $article = $this->get('shorg.article')->detailArticle($id);

        return $this->render(
            'AppBundle:blog/article:detail.html.twig',
            array(
                'article' => $article,
            )
        );

    }

    /**
     * @Route("/delete/{id}", name="article_delete")
     *
     * @param $id
     *
     */
    public function deleteAction($id)
    {
        $this->get('shorg.article')->deleteArticle($id);

        return $this->redirectToRoute('article_list');

    }

    /**
     * @Route("/update/{id}", name="article_update")
     *
     * @param $id
     *
     */
    public function updateAction(Request $request, $id)
    {
        // GET ARTICLE
        $article = $this->get('shorg.article')->detailArticle($id);

        // CREATE FORM
        $form = $this->createForm(
            ArticleFormType::class,
            $article,
            [
                "article" => $article,
            ]
        );

        // HANDLE FORM
        $form->handleRequest($request);

        // PERSIST IF FORM IS SUBMITTED
        if ($form->isSubmitted()) {

            // IF VALID
            if ($form->isValid()) {

                // Persist
                $this->get('shorg.article')->persistExistingArticle($article);
            }
        }

        // RETURN
        return $this->render(
            'AppBundle:blog/article:update.html.twig',
            array(
                'form'    => $form->createView(),
                'article' => $article,
            )
        );
    }

    /**
     * @Route("/create", name="article_create")
     */
    public function createAction(Request $request)
    {

        // CREATE ARTICLE
        $article = new Article();

        // CREATE FORM
        $form = $this->createForm(
            ArticleFormType::class,
            $article,
            [
                "article" => $article,
            ]
        );

        // HANDLE FORM
        $form->handleRequest($request);

        // PERSIST IF FORM IS SUBMITTED
        if ($form->isSubmitted()) {

            // IF VALID
            if ($form->isValid()) {

                // Persist
                $this->get('shorg.article')->persistNewArticle($article);

                return $this->redirectToRoute('article_list');
            }
        }


        // Display
        return $this->render(
            'AppBundle:blog/article:create.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }
}
