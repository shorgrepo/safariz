<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/game", name="safariz_iggame")
 */
class IgGameController extends Controller
{
    /**
     * @Route("/play", name="safariz_iggame_play")
     */
    public function playAction()
    {
        $player = $this->get('ig.player.manager')->getOne(1);
        $game = $this->get('ig.game.manager')->getOne(1);
        $res  = $this->get('ig.game.handler')->play($game, $player);


        return $this->render(
            '@App/game/result.html.twig',
            [
                'result' => null === $res->getPrize() ? false : true
            ]
        );
    }

    /**
     * @Route("/win", name="safariz_iggame_win")
     */
    public function winAction()
    {

    }

    /**
     * @Route("/loose", name="safariz_iggame_loose")
     */
    public function looseAction()
    {

    }

    /**
     * @Route("/close", name="safariz_iggame_close")
     */
    public function closeAction()
    {

    }

}
