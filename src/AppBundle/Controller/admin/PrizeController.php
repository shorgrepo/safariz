<?php

namespace AppBundle\Controller\admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shorg\IgBundle\Entity\Prize;
use Shorg\IgBundle\Form\Type\PrizeCreateFormType;
use Shorg\IgBundle\Form\Type\PrizeEditFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LotController
 *
 * @Route("/admin/prize")
 *
 */
class PrizeController extends Controller
{
    /**
     * @Route("/list", name="safariz_admin_prize_list")
     */
    public function listAction()
    {
        // Return
        return $this->render(
            '@App/admin/prize/list.html.twig',
            [
                'prizes' => $this->container->get('ig.prize.manager')->getList(),
            ]
        );
    }

    /**
     * @Route("/detail/{id}", name="safariz_admin_prize_detail")
     */
    public function detailAction($id)
    {
        // Get detail
        $manager = $this->container->get('ig.prize.manager');
        $prize = $manager->getOne($id);

        // Return
        return $this->render(
            '@App/admin/prize/detail.html.twig',
            [
                'prize' => $prize,
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="safariz_admin_prize_delete")
     */
    public function deleteAction($id)
    {
        // Delete
        $manager = $this->container->get('ig.prize.manager');
        $manager->deleteById($id);

        // Return
        return $this->redirectToRoute("safariz_admin_prize_list");


    }

    /**
     * @Route("/create}", name="safariz_admin_prize_create")
     */
    public function createAction(Request $request)
    {
        // Create new Prize
        $prize = new Prize();

        // Create form
        $form = $this->createForm(
            PrizeEditFormType::class,
            $prize
        );
        $form->add(
            $this->get('translator')->trans('action.create', [], 'default'),
            SubmitType::class
        );

        // Handle form
        $form->handleRequest($request);

        // Persist if form is submitted
        if ($form->isSubmitted()) {

            // If valid
            if ($form->isValid()) {

                // Persist
                $this->get('ig.prize.manager')->persistNewElement($prize);

                // Redirect
                return $this->redirectToRoute('safariz_admin_prize_list');
            }
        }

        // Return
        return $this->render(
            '@App/admin/prize/create.html.twig',
            [
                'monformulaire' => $form->createView(),
            ]
        );

    }


    /**
     * @Route("/update/{id}", name="safariz_admin_prize_update")
     */
    public function updateAction(Request $request, $id)
    {
        // Get Prize
        $prize = $this->get('ig.prize.manager')->getOne($id);

        // Create form
        $form = $this->createForm(
            PrizeEditFormType::class,
            $prize
        );
        $form->add(
            $this->get('translator')->trans('action.update', [], 'default'),
            SubmitType::class
        );

        // Handle form
        $form->handleRequest($request);

        // Persist if form is submitted
        if ($form->isSubmitted()) {

            // If valid
            if ($form->isValid()) {

                // Persist
                $this->get('ig.prize.manager')->persistNewElement($prize);

                // Redirect
                return $this->redirectToRoute('safariz_admin_prize_list');
            }
        }

        // Return
        return $this->render(
            '@App/admin/prize/update.html.twig',
            [
                'monformulaire' => $form->createView(),
                'prize'         => $prize,
            ]
        );
    }

}
