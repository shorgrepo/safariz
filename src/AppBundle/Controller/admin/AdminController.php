<?php

namespace AppBundle\Controller\admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AdminController
 *
 * @Route("/admin")
 *
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="safariz_admin_home")
     */
    public function homeAction()
    {
        return $this->render(
            '@App/admin/home.html.twig'
        );
    }
}
