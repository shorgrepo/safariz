<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="safariz_home")
     */
    public function homeAction()
    {
        return $this->render(
            '@App/default/home.html.twig'
        );
    }

    /**
     * @Route("/terms", name="safariz_terms")
     */
    public function termsAction()
    {
        return $this->render(
            '@App/default/terms.html.twig'
        );
    }

    /**
     * @Route("/contact", name="safariz_contact")
     */
    public function contactAction()
    {
        return $this->render(
            '@App/default/contact.html.twig'
        );
    }

    /**
     * @Route("/about", name="safariz_about")
     */
    public function aboutAction()
    {
        return $this->render(
            '@App/default/about.html.twig'
        );
    }
}
