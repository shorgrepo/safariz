<?php

namespace Shorg\IgBundle\Handler;

use Shorg\IgBundle\Entity\Essay;
use Shorg\IgBundle\Entity\Game;
use Shorg\IgBundle\Entity\Player;
use Shorg\IgBundle\Manager\EssayManager;
use Shorg\IgBundle\Manager\PrizeManager;

/**
 * Class GameHandler
 */
class GameHandler
{
    /**
     * @var EssayManager
     */
    private $essayManager;

    /**
     * @var PrizeManager
     */
    private $prizeManager;

    /**
     * GameHandler constructor.
     *
     * @param EssayManager $essayManager
     * @param PrizeManager $prizeManager
     */
    public function __construct(EssayManager $essayManager, PrizeManager $prizeManager)
    {
        $this->essayManager = $essayManager;
        $this->prizeManager = $prizeManager;
    }

    /**
     * @param Game   $game
     * @param Player $player
     *
     * @return Essay
     *
     * @throws \Exception
     */
    public function play(Game $game, Player $player)
    {
        // Check game authorization
        $check = $this->checkAuthorization($game);
        if (false === $check) {
            throw new \Exception("The game is not available to play");
        }

        // Get a prize available
        $prize = $this->prizeManager->getNextPrizeAvailable($game);

        // Create Essay
        $date = new \DateTime();
        $essay = new Essay();
        $essay->setDate($date);
        $essay->setGame($game);
        $essay->setPlayer($player);
        if (null !== $prize) {
            $essay->setPrize($prize);
        }

        // Persist Essay
        $this->essayManager->persistNewElement($essay);

        // Return
        return $essay;

    }

    /**
     * @param Game $game
     *
     * @return bool
     */
    public function checkAuthorization(Game $game)
    {
        // Check if open
        if (true === $this->isOpen($game)) {
            return true;
        }

        return false;
    }

    /**
     * @param Game $game
     *
     * @return int const Game::SATUS_...
     */
    public function getStatus(Game $game)
    {
        // Open
        if ($this->isOpen($game)) {
            return Game::STATUS_OPEN;
        }

        // Soon
        if ($this->isCommingSoon($game)) {
            return Game::STATUS_SOON;
        }

        // Close
        if ($this->isClose($game)) {
            return Game::STATUS_CLOSE;
        }

    }

    /**
     * Is open
     *
     * @return boolean
     */
    public function isOpen(Game $game)
    {
        $date = new \DateTime('now');

        if ($date->format('Y-m-d H:i:s') >= $game->getDateStart()->format('Y-m-d H:i:s')) {
            return true;
        }

        return false;

    }

    /**
     * Is close
     *
     * @return boolean
     */
    public function isClose(Game $game)
    {
        $date = new DateTime('now');

        if ($date->format('Y-m-d H:i:s') > $game->getDateEnd()->format('Y-m-d H:i:s')) {
            return true;
        }

        return false;

    }

    /**
     * Is comming soon
     *
     * @return boolean
     */
    public function isCommingSoon(Game $game)
    {
        $date = new DateTime('now');

        if ($date->format('Y-m-d H:i:s') < $game->getDateStart()->format('Y-m-d H:i:s')) {
            return true;
        }

        return false;

    }


}

