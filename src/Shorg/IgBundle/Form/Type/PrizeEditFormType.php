<?php

namespace Shorg\IgBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PrizeEditFormType
 *
 * @package Shorg\IgBundle\Form\Type
 */
class PrizeEditFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'game',
                EntityType::class,
                [
                    'class'         => 'ShorgIgBundle:Game',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('game')
                            ->orderBy('game.label', 'ASC');
                    },
                    'choice_label'  => 'label',
                ]
            )
            ->add(
                'category',
                EntityType::class,
                [
                    'class'         => 'ShorgIgBundle:PrizeCategory',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('prizeCateogry')
                            ->orderBy('prizeCateogry.label', 'ASC');
                    },
                    'choice_label'  => 'label',
                ]
            )
            ->add(
                'dateStart',
                DateTimeType::class,
                [
                    'widget' => 'single_text',
                ]
            );

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'prize_edit';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefault(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Shorg\IgBundle\Entity\Prize',
            )
        );
    }
}