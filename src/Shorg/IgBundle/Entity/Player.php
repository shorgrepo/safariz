<?php
namespace Shorg\IgBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Jeu
 *
 * @ORM\Entity(repositoryClass="Shorg\IgBundle\Entity\Repository\PlayerRepository")
 * @ORM\Table(name="player")
 *
 * @package Shorg\IgBundle\Entity
 */
class Player
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=48)
     */
    private $lastname;

    /**
     * @var string
     * @ORM\Column(name="firstname", type="string", length=48)
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", length=48)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(name="zip_code", type="string", length=5)
     */
    private $zipCode;

    /**
     * @var string
     * @ORM\Column(name="town", type="string", length=48)
     */
    private $town;

    /**
     * @var string
     * @ORM\Column(name="phone_number", type="string", length=10)
     */
    private $phoneNumber;

    /**
     * @var string
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var boolean
     * @ORM\Column(name="terms", type="boolean")
     */
    private $terms;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Shorg\IgBundle\Entity\Essay", mappedBy="player")
     */
    private $essays;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->essays = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Player
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Player
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Player
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return Player
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    
        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set town
     *
     * @param string $town
     * @return Player
     */
    public function setTown($town)
    {
        $this->town = $town;
    
        return $this;
    }

    /**
     * Get town
     *
     * @return string 
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Player
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    
        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Player
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set terms
     *
     * @param boolean $terms
     * @return Player
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    
        return $this;
    }

    /**
     * Get terms
     *
     * @return boolean 
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Add essays
     *
     * @param \Shorg\IgBundle\Entity\Essay $essays
     * @return Player
     */
    public function addEssay(\Shorg\IgBundle\Entity\Essay $essays)
    {
        $this->essays[] = $essays;
    
        return $this;
    }

    /**
     * Remove essays
     *
     * @param \Shorg\IgBundle\Entity\Essay $essays
     */
    public function removeEssay(\Shorg\IgBundle\Entity\Essay $essays)
    {
        $this->essays->removeElement($essays);
    }

    /**
     * Get essays
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEssays()
    {
        return $this->essays;
    }
}
