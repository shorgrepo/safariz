<?php
namespace Shorg\IgBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class Game
 *
 * @ORM\Entity(repositoryClass="Shorg\IgBundle\Entity\Repository\GameRepository")
 * @ORM\Table(name="game")
 *
 * @package Shorg\IgBundle\Entity
 */
class Game
{
    const STATUS_CLOSE = 0;
    const STATUS_OPEN = 1;
    const STATUS_SOON = 2;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var DateTime
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $dateStart;

    /**
     * @var DateTime
     * @ORM\Column(name="date_end", type="datetime")
     */
    private $dateEnd;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Shorg\IgBundle\Entity\Essay", mappedBy="game")
     */
    private $essays;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Shorg\IgBundle\Entity\Prize", mappedBy="game")
     */
    private $prizes;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->essays = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Game
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Game
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Game
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Add essays
     *
     * @param \Shorg\IgBundle\Entity\Essay $essays
     * @return Game
     */
    public function addEssay(\Shorg\IgBundle\Entity\Essay $essays)
    {
        $this->essays[] = $essays;
    
        return $this;
    }

    /**
     * Remove essays
     *
     * @param \Shorg\IgBundle\Entity\Essay $essays
     */
    public function removeEssay(\Shorg\IgBundle\Entity\Essay $essays)
    {
        $this->essays->removeElement($essays);
    }

    /**
     * Get essays
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEssays()
    {
        return $this->essays;
    }

    /**
     * Add prizes
     *
     * @param \Shorg\IgBundle\Entity\Prize $prizes
     * @return Game
     */
    public function addPrize(\Shorg\IgBundle\Entity\Prize $prizes)
    {
        $this->prizes[] = $prizes;
    
        return $this;
    }

    /**
     * Remove prizes
     *
     * @param \Shorg\IgBundle\Entity\Prize $prizes
     */
    public function removePrize(\Shorg\IgBundle\Entity\Prize $prizes)
    {
        $this->prizes->removeElement($prizes);
    }

    /**
     * Get prizes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrizes()
    {
        return $this->prizes;
    }
}
