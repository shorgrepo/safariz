<?php

namespace Shorg\IgBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Prize
 *
 * @ORM\Entity(repositoryClass="Shorg\IgBundle\Entity\Repository\PrizeRepository")
 * @ORM\Table(name="prize")
 *
 * @package Shorg\IgBundle\Entity
 */
class Prize
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var PrizeCategory
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="Shorg\IgBundle\Entity\PrizeCategory", inversedBy="prizes")
     */
    private $category;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $dateStart;

    /**
     * @var Game
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="Shorg\IgBundle\Entity\Game", inversedBy="prizes")
     */
    private $game;

    /**
     * @var Essay
     * @ORM\OneToOne(targetEntity="Shorg\IgBundle\Entity\Essay", mappedBy="prize")
     */
    private $essay;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return Prize
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return PrizeCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Prize
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set essay
     *
     * @param \Shorg\IgBundle\Entity\Essay $essay
     *
     * @return Prize
     */
    public function setEssay(\Shorg\IgBundle\Entity\Essay $essay = null)
    {
        $this->essay = $essay;

        return $this;
    }

    /**
     * Get essay
     *
     * @return \Shorg\IgBundle\Entity\Essay
     */
    public function getEssay()
    {
        return $this->essay;
    }

    /**
     * Set game
     *
     * @param integer $game
     *
     * @return Prize
     */
    public function setGame($game)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }


}
