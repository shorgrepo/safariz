<?php
namespace Shorg\IgBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PrizeCategory
 *
 * @ORM\Entity(repositoryClass="Shorg\IgBundle\Entity\Repository\PrizeCategoryRepository")
 * @ORM\Table(name="prize_category")
 *
 * @package Shorg\IgBundle\Entity
 */
class PrizeCategory
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(
     *     min="3",
     *     max="10",
     * )
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Shorg\IgBundle\Entity\Prize", mappedBy="category")
     */
    private $prizes;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prizes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return PrizeCategory
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Add prizes
     *
     * @param \Shorg\IgBundle\Entity\Prize $prizes
     * @return PrizeCategory
     */
    public function addPrize(\Shorg\IgBundle\Entity\Prize $prizes)
    {
        $this->prizes[] = $prizes;
    
        return $this;
    }

    /**
     * Remove prizes
     *
     * @param \Shorg\IgBundle\Entity\Prize $prizes
     */
    public function removePrize(\Shorg\IgBundle\Entity\Prize $prizes)
    {
        $this->prizes->removeElement($prizes);
    }

    /**
     * Get prizes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrizes()
    {
        return $this->prizes;
    }
}
