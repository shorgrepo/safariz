<?php
namespace Shorg\IgBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class Essay
 *
 * @ORM\Entity(repositoryClass="Shorg\IgBundle\Entity\Repository\EssayRepository")
 * @ORM\Table(name="essay")
 *
 * @package Shorg\IgBundle\Entity
 */
class Essay
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Game
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="Shorg\IgBundle\Entity\Game", inversedBy="essays")
     */
    private $game;

    /**
     * @var Player
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="Shorg\IgBundle\Entity\Player", inversedBy=""))
     */
    private $player;

    /**
     * @var DateTime
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var Prize
     * @ORM\JoinColumn(name="prize_id", referencedColumnName="id", nullable=true)
     * @ORM\OneToOne(targetEntity="Shorg\IgBundle\Entity\Prize", inversedBy="essay")
     */
    public $prize;

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set game
     *
     * @param Game $game
     * @return Essay
     */
    public function setGame(Game $game)
    {
        $this->game = $game;
    
        return $this;
    }

    /**
     * Get game
     *
     * @return integer 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set player
     *
     * @param Player $player
     * @return Essay
     */
    public function setPlayer(Player $player)
    {
        $this->player = $player;
    
        return $this;
    }

    /**
     * Get player
     *
     * @return integer 
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Essay
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set prize
     *
     * @param Prize $prize
     * @return Essay
     */
    public function setPrize(Prize $prize)
    {
        $this->prize = $prize;
    
        return $this;
    }

    /**
     * Get prize
     *
     * @return Prize
     */
    public function getPrize()
    {
        return $this->prize;
    }
}
