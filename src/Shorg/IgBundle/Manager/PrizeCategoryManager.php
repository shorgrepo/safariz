<?php

namespace Shorg\IgBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Shorg\IgBundle\Entity\PrizeCategory;

/**
 * Class PrizeCategoryManager
 *
 * @package Shorg\IgBundle\Manager
 */
class PrizeCategoryManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $er;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->er = $doctrine->getRepository('ShorgIgBundle:PrizeCategory'); //
    }

    /**
     * get list of prizeCategorys
     *
     * @return array|PrizeCategory[]
     */
    public function getList()
    {
        return $this->er->findAll();

    }

    /**
     * Get one
     *
     * @param $id
     *
     * @return object|\Shorg\IgBundle\Entity\PrizeCategory
     */
    public function getOne($id)
    {
        return $this->er->find($id);
    }

    /**
     * Persist New Element
     *
     * @param PrizeCategory $element
     */
    public function persistNewElement(PrizeCategory $element)
    {
        $this->persist($element);
    }

    /**
     * Persist Existing Element
     *
     * @param PrizeCategory $element
     */
    public function persistExistingElement(PrizeCategory $element)
    {
        $this->persist($element);
    }

    /**
     * Delete
     *
     * @param $id
     */
    public function delete($id)
    {
        $this->em->remove($this->getDetail($id));
        $this->em->flush();
    }

    /**
     * Persist
     *
     * @param PrizeCategory $element
     */
    private function persist(PrizeCategory $element)
    {
        $this->em->persist($element);
        $this->em->flush();
    }

}
 
