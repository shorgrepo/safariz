<?php

namespace Shorg\IgBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Shorg\IgBundle\Entity\Player;

/**
 * Class PlayerManager
 *
 * @package Shorg\IgBundle\Manager
 */
class PlayerManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $er;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->er = $doctrine->getRepository('ShorgIgBundle:Player'); //
    }

    /**
     * get list of players
     *
     * @return array|Player[]
     */
    public function getList()
    {
        return $this->er->findAll();
    }

    /**
     * Get one
     *
     * @param $id
     *
     * @return object|\Shorg\IgBundle\Entity\Player
     */
    public function getOne($id)
    {
        return $this->er->find($id);
    }

    /**
     * Persist New Element
     *
     * @param Player $element
     */
    public function persistNewElement(Player $element)
    {
        $this->persist($element);
    }

    /**
     * Persist Existing Element
     *
     * @param Player $element
     */
    public function persistExistingElement(Player $element)
    {
        $this->persist($element);
    }

    /**
     * Delete
     *
     * @param $id
     */
    public function delete($id)
    {
        $this->em->remove($this->getDetail($id));
        $this->em->flush();
    }

    /**
     * Persist
     *
     * @param Player $element
     */
    private function persist(Player $element)
    {
        $this->em->persist($element);
        $this->em->flush();
    }

}
 
