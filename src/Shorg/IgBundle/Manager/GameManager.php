<?php

namespace Shorg\IgBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Shorg\IgBundle\Entity\Game;

/**
 * Class GameManager
 *
 * @package Shorg\IgBundle\Manager
 */
class GameManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $er;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->er = $doctrine->getRepository('ShorgIgBundle:Game'); //
    }

    /**
     * get list of games
     *
     * @return array|Game[]
     */
    public function getList()
    {
        return $this->er->findAll();
    }

    /**
     * Get one
     *
     * @param $id
     *
     * @return object|\Shorg\IgBundle\Entity\Game
     */
    public function getOne($id)
    {
        return $this->er->find($id);
    }

    /**
     * Persist New Element
     *
     * @param Game $element
     */
    public function persistNewElement(Game $element)
    {
        $this->persist($element);
    }

    /**
     * Persist Existing Element
     *
     * @param Game $element
     */
    public function persistExistingElement(Game $element)
    {
        $this->persist($element);
    }

    /**
     * Delete
     *
     * @param $id
     */
    public function delete($id)
    {
        $this->em->remove($this->getDetail($id));
        $this->em->flush();
    }

    /**
     * Persist
     *
     * @param Game $element
     */
    private function persist(Game $element)
    {
        $this->em->persist($element);
        $this->em->flush();
    }

}
 
