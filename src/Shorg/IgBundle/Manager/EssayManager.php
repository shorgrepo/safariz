<?php

namespace Shorg\IgBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Shorg\IgBundle\Entity\Essay;
use Shorg\IgBundle\Entity\Game;
use Shorg\IgBundle\Entity\Player;

/**
 * Class EssayManager
 *
 * @package Shorg\IgBundle\Manager
 */
class EssayManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $er;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->er = $doctrine->getRepository('ShorgIgBundle:Essay');
    }

    /**
     * @param Game   $game
     * @param Player $player
     * @param Essay  $essay
     */
    public function updateWithPrize(Game $game, Player $player, Essay $essay)
    {
        $this->er->updateWithPrize($game, $player, $essay);
    }

    /**
     * get list of essays
     *
     * @return array|Essay[]
     */
    public function getList()
    {
        return $this->er->findAll();
    }

    /**
     * Get one
     *
     * @param $id
     *
     * @return object|\Shorg\IgBundle\Entity\Essay
     */
    public function getOne($id)
    {
        return $this->er->find($id);
    }

    /**
     * Persist New Element
     *
     * @param Essay $element
     */
    public function persistNewElement(Essay $element)
    {
        $this->persist($element);
    }

    /**
     * Persist Existing Element
     *
     * @param Essay $element
     */
    public function persistExistingElement(Essay $element)
    {
        $this->persist($element);
    }

    /**
     * Delete
     *
     * @param $id
     */
    public function delete($id)
    {
        $this->em->remove($this->getOne($id));
        $this->em->flush();
    }

    /**
     * Persist
     *
     * @param Essay $element
     */
    private function persist(Essay $element)
    {
        $this->em->persist($element);
        $this->em->flush();
    }

}
 
