<?php

namespace Shorg\IgBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Shorg\IgBundle\Entity\Game;
use Shorg\IgBundle\Entity\Prize;

/**
 * Class PrizeManager
 *
 * @package Shorg\IgBundle\Manager
 */
class PrizeManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $er;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->er = $doctrine->getRepository('ShorgIgBundle:Prize');
    }


    /**
     * get next prize available
     *
     * @param Game $game
     *
     * @return Prize|null
     */
    public function getNextPrizeAvailable(Game $game)
    {
        $list = $this->er->findBy(
            ['game' => $game],
            ['dateStart' => 'asc']
        );

        /** @var Prize $prize */
        foreach ($list as $prize) {
            if ($prize->getEssay() == null) {
                return $prize;
            }
        }

        return null;


    }

    /**
     * get list of prizes
     *
     * @return array|Prize[]
     */
    public function getList()
    {
        return $this->er->findAll();

    }

    /**
     * Get one
     *
     * @param $id int
     *
     * @return object|\Shorg\IgBundle\Entity\Prize
     */
    public function getOne($id)
    {
        return $this->er->find($id);
    }

    /**
     * Persist New Element
     *
     * @param Prize $element
     */
    public function persistNewElement(Prize $element)
    {
        $this->persist($element);
    }

    /**
     * Persist Existing Element
     *
     * @param Prize $element
     */
    public function persistExistingElement(Prize $element)
    {
        $this->persist($element);
    }

    /**
     * Delete
     *
     * @param $id
     */
    public function deleteById($id)
    {
        $this->em->remove($this->getOne($id));
        $this->em->flush();
    }

    /**
     * Persist
     *
     * @param Prize $element
     */
    private function persist(Prize $element)
    {
        $this->em->persist($element);
        $this->em->flush();
    }

}
 
